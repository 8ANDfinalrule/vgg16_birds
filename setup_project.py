# !/usr/bin/python
# -*- coding: utf-8 -*-

# The MIT License (MIT)
# Copyright (c) 2018 Daniel Koguciuk <daniel.koguciuk@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

'''
@author: Daniel Koguciuk <daniel.koguciuk@gmail.com>
@note: Created on 23.03.2018
'''

import os
import tarfile
import requests
from tqdm import tqdm

CALTECH_DIR = 'CUB_200_2011.tgz'
CALTECH_URL = 'http://www.vision.caltech.edu/visipedia-data/CUB-200-2011/CUB_200_2011.tgz'
ALEXNET_DIR = 'weights/bvlc_alexnet.npy'
ALEXNET_URL = 'https://www.cs.toronto.edu/~guerzhoy/tf_alexnet/bvlc_alexnet.npy'
VGG_DIR = 'weights/vgg16_weights.npz'
VGG_URL = 'https://www.cs.toronto.edu/~frossard/vgg16/vgg16_weights.npz'

def download_model_weights(url):
    """
    Download  model weights archive from specified url and save it as filename.
    Args:
        url (str): URL to the inception model archive.
    """
    CHUNK_SIZE = 1024

    archive_name = url[url.rfind('/') + 1:]
    r_head = requests.head(url, stream=True)
    filesize = int(r_head.headers['Content-length'])

    r_get = requests.get(url, stream=True)
    with open(os.path.join('./weights', archive_name), 'wb') as f:
        for chunk in tqdm(r_get.iter_content(chunk_size=CHUNK_SIZE), total=filesize / CHUNK_SIZE):
            if chunk:
                f.write(chunk)


def download_caltech(url, filename=None):
    """
        Download caltech archive from specified url and save it as filename.
        Args:
            url (str): URL to the caltech archive.
            filename (str): How should I name local archive? Will be extracted
                from url if not provided.
        Returns:
            (str): Local archive filename.
    """
    CHUNK_SIZE = 1024

    if filename == None:
        filename = url[url.rfind('/') + 1:]

    r_head = requests.head(url, stream=True)
    filesize = int(r_head.headers['Content-length'])

    r_get = requests.get(url, stream=True)
    with open(filename, 'wb') as f:
        for chunk in tqdm(r_get.iter_content(chunk_size=CHUNK_SIZE), total=filesize / CHUNK_SIZE):
            if chunk:
                f.write(chunk)

    return filename


def extract_caltech(filename, delete_archive=True):
    """
    Extract cifar archive with specified name.

    Args:
        filename (str): Local archive filename.
        delete_archive (bool): Should I delete archive after extracting?
    """
    if (filename.endswith(".tgz")):
        def members(tf):
            l = len("CUB_200_2011/")
            for member in tf.getmembers():
                if member.path.startswith("CUB_200_2011/images"):
                    member.path = member.path[l:]
                    yield member
        print('Extracting images ...')
        with tarfile.open(filename) as tar:
            tar.extractall(members=members(tar), path='./data')

        if not os.path.exists(CALTECH_DIR):
            raise ValueError("Extracting error..")

        if delete_archive:
            os.remove(filename)
    else:
        raise ValueError("Sorry, don't know this archive extension.")


def setup(ROOT_DIR, model_name):
    if model_name == 'alex':
        url = ALEXNET_URL
        MODEL_DIR = os.path.join(ROOT_DIR, ALEXNET_DIR)
    elif model_name == 'vgg':
        url = VGG_URL
        MODEL_DIR = os.path.join(ROOT_DIR, VGG_DIR)
    else:
        raise ValueError("Unknown model")

    if not os.path.exists(MODEL_DIR):
        if not os.path.exists(os.path.join(ROOT_DIR, 'weights')):
            os.mkdir('weights')
        print("Downloading model weights...")
        download_model_weights(url=url)

    else:
        print("Model weights already downloaded!")

    IMAGES_DIR = os.path.join(ROOT_DIR, 'data', 'images')
    if not os.path.exists(IMAGES_DIR):

        print("Downloading dataset...")
        filename = download_caltech(CALTECH_URL, filename=CALTECH_DIR)

        print("Extracting dataset...")
        extract_caltech(filename, delete_archive=True)

        print("All done!")

    else:
        print("Dataset already downloaded!")


