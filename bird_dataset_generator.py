
import numpy as np
import os
import csv
import random
from scipy import misc


def to_categorical(y,num_classes=200):
    y = np.array(y, dtype='int').ravel()
    n = y.shape[0]
    categorical = np.zeros((n, num_classes))
    categorical[np.arange(n), y] = 1
    return categorical


class BirdClassificationGenerator(object):
    def __init__(self, dataset_path, validation_ratio=0.3, batch_size=16):
        self.dataset_path = dataset_path
        self.batch_size = batch_size
        self.num_classes = 200
 
        self.train_list = []
        self.test_list = []
        
        self.bb_bird_dict = {}
        self.images_dict = {}
        
        self.train_labels_dict = {}
        
        with open(os.path.join(dataset_path, 'images_train.txt'), 'r') as f:
            spamreader = csv.reader(f, delimiter=' ')
            for row in spamreader:
                self.train_list.append(int(row[0]))
                self.images_dict[int(row[0])] = os.path.join('images',row[1])

        with open(os.path.join(dataset_path, 'bounding_boxes_train.txt')) as f:
            spamreader = csv.reader(f, delimiter=' ')
            for row in spamreader:
                self.bb_bird_dict[int(row[0])] = [ int(float(x)) for x in row[1:5] ]
         
        with open(os.path.join(dataset_path, 'image_class_labels_train.txt')) as f:
            spamreader = csv.reader(f, delimiter=' ')
            for row in spamreader:
                self.train_labels_dict[int(row[0])] = int(row[1]) - 1 # offset by 0
                # self.train_labels_dict[int(row[0])] = (int(row[1]) -1 )/ 20 #offset by 0

        with open(os.path.join(dataset_path, 'images_test.txt')) as f:
            spamreader = csv.reader(f, delimiter=' ')
            for row in spamreader:
                self.test_list.append(int(row[0]))
                self.images_dict[int(row[0])] = os.path.join('images',row[1])
                       
        with open(os.path.join(dataset_path, 'bounding_boxes_test.txt')) as f:
            spamreader = csv.reader(f, delimiter=' ')
            for row in spamreader:
                self.bb_bird_dict[int(row[0])] = [ int(float(x)) for x in row[1:5] ]
        
        size_val_list = int(validation_ratio*len(self.train_list))
        self.val_list = random.sample(self.train_list, size_val_list)
        print ('VAL LIST', len(self.val_list))
        self.train_list = list(filter(lambda x: x not in self.val_list, self.train_list))
        print ('TRAIN LIST', len(self.train_list))
        print('TEST LIST', len(self.test_list))

        # Coz Gradient Descent
        # random.shuffle(self.val_list) # not needed
        random.shuffle(self.train_list)
        # random.shuffle(self.test_list) # not needed

    def _shuffle(self):
        # random.shuffle(self.val_list) # not needed
        random.shuffle(self.train_list)
        
    def _generate(self, idx_list, set_type):
        loop = 0
        max_size = len(idx_list)
        while True:
            if loop + self.batch_size < max_size:
                gen_list = idx_list[loop:loop+self.batch_size]
            else:
                last_iter = loop + self.batch_size - max_size
                gen_list = idx_list[loop:max_size] 
                loop = 0
                self._shuffle()

            loop += self.batch_size
            assert(len(gen_list) <= self.batch_size)
            if set_type == 'train' or set_type =='val':
                yield ([ os.path.join(self.dataset_path, self.images_dict[x]) for x in gen_list ], 
                          np.array([ self.bb_bird_dict[x] for x in gen_list ]), 
                          to_categorical([ self.train_labels_dict[x] for x in gen_list ]))
            else:
                yield ([ os.path.join(self.dataset_path, self.images_dict[x]) for x in gen_list ], 
                           np.array([ self.bb_bird_dict[x] for x in gen_list ]))

    def train_generator(self):
        return self._generate(self.train_list, 'train')
 
    def test_generator(self): 
        return self._generate(self.test_list, 'test')

    def val_generator(self):
        return self._generate(self.val_list, 'val')


'''
import cv2


# pre-processing stuff
def gray2rgb(img, path):
    if len(img.shape) < 3:
        img = np.stack((img,)*3,axis=2)
    return img


def random_flip_lr(img):
    rand_num = np.random.rand(1)
    if rand_num > 0.5:
        img = np.flip(img, 1)
    return img


def random_brightness(img):
    rand_num = np.random.randint(8, high=12, size=1)/10.0
    img = img * rand_num;
    img[img > 255] = 255
    img = img.astype(dtype=np.uint8)
    return img


def normalize_input(img, height):
    img = img.astype(dtype=np.float32)
    img[:,:,0] -= 103.939
    img[:,:,1] -= 116.779
    img[:,:,2] -= 123.68
    # img = np.divide(img, 255.0)
    return img


def add_random_noise(img):
    return img + np.random.normal(0, 50.0, (img.shape))


def resize_to_square(img, square_size):
    min_size = min(img.shape[:2])
    scale = min_size / float(square_size)
    img = cv2.resize(img, (int(img.shape[1] // scale + 1), int(img.shape[0] // scale + 1)))
    if img.shape[0] >= img.shape[1]:
        difference = (img.shape[0] - square_size) // 2
        img = img[difference:difference + square_size, :square_size, :]
    else:
        difference = (img.shape[1] - square_size) // 2
        img = img[:square_size, difference:square_size + difference, :]
    return img


def random_crop(img, sqare_size):
    w, h = img.shape[:2]
    possible_w = w - sqare_size
    possible_h = h - sqare_size
    x = random.randint(0, possible_w - 1)
    y = random.randint(0, possible_h - 1)
    img = img[x: x + sqare_size, y: y + sqare_size, :]
    return img


def preprocess_image(img, height, width, set_type, processing_type, bbs):

    # rescale to height, width
    if processing_type == 1:
        img = img[bbs[1]:bbs[1] + bbs[3], bbs[0]:bbs[0] + bbs[2], :]
        img = cv2.resize(img, (height, width))

    # isotropically scaled and filled with black to size of net input
    elif processing_type == 2:
        img = img[bbs[1]:bbs[1] + bbs[3], bbs[0]:bbs[0] + bbs[2], :]
        zeros = np.zeros((height, width, 3), dtype=np.uint8)
        max_size = max(img.shape)
        scale = max_size / height
        img = cv2.resize(img, (int(img.shape[1] // scale), int(img.shape[0] // scale)))
        if img.shape[0] >= img.shape[1]:
            difference = (height - img.shape[1]) // 2
            zeros[:img.shape[0], difference:img.shape[1] + difference, :] = img
        else:
            difference = (height - img.shape[0]) // 2
            zeros[difference:img.shape[0] + difference, :img.shape[1], :] = img
        img = zeros

    # isotropically scaled to and cropped from centre to fit net input
    elif processing_type == 3:
        img = resize_to_square(img, height)

    # Cropped bounding box, scaled to S=256 and random crop
    elif processing_type == 4:
        S = 256
        img = img[bbs[1]:bbs[1] + bbs[3], bbs[0]:bbs[0] + bbs[2], :]
        min_size = min(img.shape[:2])
        print('size ', min_size)
        scale = min_size / float(S)
        print('min: ', min_size, ' scale: ', scale)
        img = cv2.resize(img, (int(img.shape[1] // scale + 1),
                               int(img.shape[0] // scale + 1)))
        img = random_crop(img, height)

    # Cropped bounding box, scaled to S=(256, 512) and random crop
    elif processing_type == 5:
        S = np.random.randint(226, high=448, size=1)
        img = img[bbs[1]:bbs[1] + bbs[3], bbs[0]:bbs[0] + bbs[2], :]
        min_size = min(img.shape[:2])
        scale = min_size / float(S)
        img = cv2.resize(img, (int(img.shape[1] // scale + 1),
                               int(img.shape[0] // scale + 1)))
        img = random_crop(img, height)

    # Test 1
    elif processing_type == 6:
        img = img[bbs[1]:bbs[1] + bbs[3], bbs[0]:bbs[0] + bbs[2], :]
        img = resize_to_square(img, height)

    # Test 3
    elif processing_type == 7:
        S = np.random.randint(226, high=448, size=1)
        print('S ', S)
        img = img[bbs[1]:bbs[1] + bbs[3], bbs[0]:bbs[0] + bbs[2], :]
        min_size = min(img.shape[:2])
        scale = min_size / float(S)
        img = cv2.resize(img, (int(img.shape[1] // scale + 1),
                               int(img.shape[0] // scale + 1)))
        img = resize_to_square(img, height)


    # if set_type == 'train' and processing_type not in (1, 2, 6, 7):
        # img = random_flip_lr(img)
        # img = random_brightness(img)

    # img = normalize_input(img, height)
    # img = add_random_noise(img)
    return img


# return a batch of input(images, labels) to feed into placeholders
def get_batch(generator_type, set_type, height, width, processing_type):
    imgs = []
    imgs1 = []
    if set_type == 'train' or set_type == 'val':
        for paths, bbs, labels in generator_type:
            for i in range(len(paths)):
                img = gray2rgb(misc.imread(paths[i]), paths[i])
                # img=cv2.imread(paths[i])
                img1 = preprocess_image(img, height, width,
                                       set_type, 3, bbs[i])
                img = preprocess_image(img, height, width,
                            set_type, processing_type, bbs[i])
                imgs.append(img)
                imgs1.append(img1)
            imgs = np.asarray(imgs)
            imgs1 = np.asarray(imgs1)
            print(list(l.tolist().index(1)+1 for l in labels))
            print(paths)
            break

        return imgs, labels, imgs1
    else:
        for paths, bbs in generator_type:
            for i in range(len(paths)):
                img = gray2rgb(misc.imread(paths[i]), paths[i])
                imgs.append( preprocess_image(img, height,
                            width, set_type, processing_type, bbs[i]))
            imgs = np.asarray(imgs)
            print(imgs)
            break
        return imgs, None


#store in required csv format
def save_csv(model_pred, obj):
    image_classes = {}
    with open(os.path.join('./data/image_class_labels.txt')) as f:
        spamreader = csv.reader(f, delimiter=' ')
        for row in spamreader:
            image_classes[int(row[0])] = int(row[1])

    with open('model_prediction.csv',"w") as f:
        writer = csv.writer(f, delimiter=',',  quotechar='"', quoting=csv.QUOTE_ALL)
        row = ['Id', 'Predicted category', 'Category', 'Accuracy']
        writer.writerow(row)
        correct_sum = 0
        for i in range(len(obj.test_list)):
            row = []
            row.append(obj.test_list[i])
            model_prediction = model_pred[i]+1
            row.append(model_prediction)
            correct_prediction = image_classes.get(obj.test_list[i])
            row.append(correct_prediction)
            if correct_prediction == model_prediction:
                correct_sum = correct_sum + 1
                row.append(1)
            else:
                row.append(0)
            writer.writerow(row)
        writer.writerow(['', '', 'ACCURACY:', correct_sum / len(obj.test_list)])
        print('TEST ACCURACY: ', correct_sum / len(obj.test_list))


generator = BirdClassificationGenerator('/home/krzysiek/Praca_inz/content/3_vgg16/data',
                                        0.2, 10)
set = generator.train_generator()
for type in range(5, 6):
    print('type: ', type)
    x_batch, _, imgs1 = get_batch(set, 'train', 224, 224, processing_type=type)


    img_final = []
    img_final2 = []
    i = 0
    imgpom = []
    for batch in x_batch:
        batch = cv2.putText(batch, str(i+1), (10, 200),
                    fontFace=cv2.FONT_HERSHEY_COMPLEX,fontScale=1.5, color=(0,0,0), thickness=2)
        batch = cv2.putText(batch, str(i+1), (12, 198),
                    fontFace=cv2.FONT_HERSHEY_COMPLEX,fontScale=1.4, color=(255,255,255), thickness=2)

        if i == 0:
            img_final = batch[:, :, ::-1]
            # line = np.ones((227, 1, 3))
        else:
            if i<5:
                img_show = batch[:, :, ::-1]
                img_final = np.concatenate((img_final, img_show), axis=1)
                # img_final = np.concatenate((img_final, line), axis=1)
            else:
                if i == 5:
                    imgpom = batch[:, :, ::-1]
                else:
                    img_show = batch[:, :, ::-1]
                    imgpom = np.concatenate((imgpom, img_show), axis=1)
        i += 1

    img_final = np.concatenate((img_final, imgpom), axis=0)

    i = 0
    imgpom = []
    for batch in imgs1:
        batch = cv2.putText(batch, str(i+1), (10, 200),
                    fontFace=cv2.FONT_HERSHEY_COMPLEX,fontScale=1.5, color=(0,0,0), thickness=2)
        batch = cv2.putText(batch, str(i+1), (12, 198),
                    fontFace=cv2.FONT_HERSHEY_COMPLEX,fontScale=1.4, color=(255,255,255), thickness=2)

        if i == 0:
            img_final2 = batch[:, :, ::-1]
            # line = np.ones((227, 1, 3))
        else:
            if i<5:
                img_show = batch[:, :, ::-1]
                img_final2 = np.concatenate((img_final2, img_show), axis=1)
                # img_final = np.concatenate((img_final, line), axis=1)
            else:
                if i == 5:
                    imgpom = batch[:, :, ::-1]
                else:
                    img_show2 = batch[:, :, ::-1]
                    imgpom = np.concatenate((imgpom, img_show2), axis=1)
        i += 1
    img_final2 = np.concatenate((img_final2, imgpom), axis=0)

    cv2.imshow('win', img_final)
    cv2.imshow('win2', img_final2)

    cv2.waitKey(0)




# import cv2
# for batch in x_batch:
#     im = batch[:, :, ::-1]
#     cv2.imshow('after', im)
#     cv2.waitKey(700)

# list_paths.sort()
# from itertools import groupby
# sorted_list = [len(list(group)) for key, group in groupby(list_paths)]
# print(sorted_list)
# for p in range(len(y_batch)):
#     print('pos :', np.where(y_batch[p] == 1))



# predictions = [0] * 10000
# save_csv(predictions, generator)

'''