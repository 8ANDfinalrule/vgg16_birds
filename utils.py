from __future__ import division
from __future__ import print_function

import cv2
import numpy as np
import csv
import os, random
from scipy import misc


# pre-processing stuff
def gray2rgb(img, path):
    if len(img.shape) < 3:
        img = np.stack((img,)*3,axis=2)
    return img


def random_flip_lr(img):
    rand_num = np.random.rand(1)
    if rand_num > 0.5:
        img = np.flip(img, 1)
    return img


def random_brightness(img):
    rand_num = np.random.randint(8, high=12, size=1)/10.0
    img = img * rand_num;
    img[img > 255] = 255
    img = img.astype(dtype=np.uint8)
    return img


def normalize_input(img, height):
    img = img.astype(dtype=np.float32)
    img[:,:,0] -= 110.246
    img[:,:,1] -= 127.475
    img[:,:,2] -= 123.884
    # img = np.divide(img, 128.0)
    return img


def add_random_noise(img):
    return img + np.random.normal(0, 50.0, (img.shape))


def resize_to_square(img, square_size):
    min_size = min(img.shape[:2])
    scale = min_size / float(square_size)
    img = cv2.resize(img, (int(img.shape[1] // scale + 1), int(img.shape[0] // scale + 1)))
    if img.shape[0] >= img.shape[1]:
        difference = (img.shape[0] - square_size) // 2
        img = img[difference:difference + square_size, :square_size, :]
    else:
        difference = (img.shape[1] - square_size) // 2
        img = img[:square_size, difference:square_size + difference, :]
    return img


def random_crop(img, sqare_size):
    w, h = img.shape[:2]
    possible_w = w - sqare_size
    possible_h = h - sqare_size
    x = random.randint(0, possible_w - 1)
    y = random.randint(0, possible_h - 1)
    img = img[x: x + sqare_size, y: y + sqare_size, :]
    return img


def preprocess_image(img, height, width, set_type, processing_type, bbs):
    # cv2.imshow('before', img)
    # cv2.waitKey(1)
    # rescale to height, width
    if processing_type == 1:
        img = img[bbs[1]:bbs[1] + bbs[3], bbs[0]:bbs[0] + bbs[2], :]
        img = cv2.resize(img, (height, width))
        # img = misc.imresize(np.asarray(img), (height, width))

    # isotropically scaled and filled with black to size of net input
    elif processing_type == 2:
        img = img[bbs[1]:bbs[1] + bbs[3], bbs[0]:bbs[0] + bbs[2], :]
        zeros = np.zeros((height, width, 3), dtype=np.uint8)
        max_size = max(img.shape)
        scale = max_size / float(height)
        img = cv2.resize(img, (int(img.shape[1] // scale), int(img.shape[0] // scale)))
        if img.shape[0] >= img.shape[1]:
            difference = (height - img.shape[1]) // 2
            zeros[:img.shape[0], difference:img.shape[1] + difference, :] = img
        else:
            difference = (height - img.shape[0]) // 2
            zeros[difference:img.shape[0] + difference, :img.shape[1], :] = img
        img = zeros

    # isotropically scaled to and cropped from centre to fit net input
    elif processing_type == 3:
        img = resize_to_square(img, height)

    # Cropped bounding box, scaled to S=256 and random crop
    elif processing_type == 4:
        S = 256
        img = img[bbs[1]:bbs[1] + bbs[3], bbs[0]:bbs[0] + bbs[2], :]
        min_size = min(img.shape[:2])
        scale = min_size / float(S)
        img = cv2.resize(img, (int(img.shape[1] // scale + 1),
                               int(img.shape[0] // scale + 1)))
        img = random_crop(img, height)

    # Cropped bounding box, scaled to S=(226, 448) and random crop
    elif processing_type == 5:
        S = np.random.randint(226, high=448, size=1)
        img = img[bbs[1]:bbs[1] + bbs[3], bbs[0]:bbs[0] + bbs[2], :]
        min_size = min(img.shape[:2])
        scale = min_size / float(S)
        img = cv2.resize(img, (int(img.shape[1] // scale + 1),
                               int(img.shape[0] // scale + 1)))
        img = random_crop(img, height)

    # Test 1
    elif processing_type == 6:
        img = img[bbs[1]:bbs[1] + bbs[3], bbs[0]:bbs[0] + bbs[2], :]
        img = resize_to_square(img, height)

    # Test 3
    elif processing_type == 7:
        S = np.random.randint(226, high=448, size=1)
        img = img[bbs[1]:bbs[1] + bbs[3], bbs[0]:bbs[0] + bbs[2], :]
        min_size = min(img.shape[:2])
        scale = min_size / float(S)
        img = cv2.resize(img, (int(img.shape[1] // scale + 1),
                               int(img.shape[0] // scale + 1)))
        img = resize_to_square(img, height)


    if set_type == 'train' and processing_type not in (1, 2, 6, 7):
        img = random_flip_lr(img)
        # img = random_brightness(img)

    img = img[:, :, ::-1]
    # img = add_random_noise(img)
    # img = np.divide(img, 255.0)
    img = normalize_input(img, height)

    # cv2.imshow('after', img)
    # cv2.waitKey(200)

    # cv2.imshow('aftern', img)
    # cv2.waitKey(1)
    # print(img)
    return img


# return a batch of input(images, labels) to feed into placeholders
def get_batch(generator_type, set_type, height, width, processing_type):
    imgs = []
    if set_type == 'train' or set_type == 'val':
        for paths, bbs, labels in generator_type:
            for i in range(len(paths)):
                img = gray2rgb(misc.imread(paths[i]), paths[i])
                # img=cv2.imread(paths[i])
                img = preprocess_image(img, height, width,
                            set_type, processing_type, bbs[i])
                imgs.append(img)
            imgs = np.asarray(imgs)
            break
        return imgs, labels
    else:
        for paths, bbs in generator_type:
            for i in range(len(paths)):
                img = gray2rgb(misc.imread(paths[i]), paths[i])
                imgs.append(preprocess_image(img, height,
                            width, set_type, processing_type, bbs[i]))
            imgs = np.asarray(imgs)
            break
        return imgs, None


# store in required csv format
def save_csv(model_pred, obj, preprocessing):
    image_classes = {}
    with open(os.path.join('./data/image_class_labels.txt')) as f:
        spamreader = csv.reader(f, delimiter=' ')
        for row in spamreader:
            image_classes[int(row[0])] = int(row[1])

    with open('model_prediction'+str(preprocessing)+'.csv',"w") as f:
        writer = csv.writer(f, delimiter=',',  quotechar='"', quoting=csv.QUOTE_ALL)
        row = ['Id', 'Predicted category', 'Category', 'Accuracy']
        writer.writerow(row)
        correct_sum = 0
        for i in range(len(obj.test_list)):
            row = []
            row.append(obj.test_list[i])
            model_prediction = model_pred[i]+1
            row.append(model_prediction)
            correct_prediction = image_classes.get(obj.test_list[i])
            row.append(correct_prediction)
            if correct_prediction == model_prediction:
                correct_sum = correct_sum + 1
                row.append(1)
            else:
                row.append(0)
            writer.writerow(row)
        writer.writerow(['', '', 'ACCURACY:', correct_sum / len(obj.test_list)])
        print('TEST ACCURACY: ', correct_sum / len(obj.test_list))
