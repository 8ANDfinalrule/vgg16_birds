from __future__ import division
from __future__ import print_function

# import important modules
import tensorflow as tf
import numpy as np
import argparse
import time
import sys
import os
import cv2

# import models
from vgg16 import vgg16
from alex import alex
from setup_project import setup

# import data generator/provider
from bird_dataset_generator import BirdClassificationGenerator

# import utility functions
from utils import get_batch, save_csv


class deep_neural_net(object):
    def __init__(self, directory, batch_size=100, val_ratio=0.2, num_epochs=1001,
                 model_name='vgg', model_weight_path=None, training_bool=0,
                 restore=0, preprocessing=1, device=None):
        if training_bool == 0:
            self.preprocessing = range(1, 2)
        elif preprocessing == 0:
            self.preprocessing = range(1, 6)
        else:
            self.preprocessing = range(preprocessing, preprocessing + 1)
        self.DIR = directory
        self.batch_size = batch_size
        self.val_ratio = val_ratio
        self.num_epochs = num_epochs
        self.val_summary = tf.Summary()
        self.training = training_bool
        if training_bool == 0:
            self.testing = 1
        else:
            self.testing = 0
        self.restore = restore
        dataset_dir = os.path.join(self.DIR, 'data')
        self.obj = BirdClassificationGenerator(dataset_dir, self.val_ratio, self.batch_size)

        if device == "None":
            self.run_training_testing(model_weight_path, model_name, device)
        else:
            with tf.device(device):
                self.run_training_testing(model_weight_path, model_name, device)

    def evaluate(self, sess, set_type, preprocessing):
        if set_type == 'val':
            num_images = len(self.obj.val_list)
            generator = self.obj.val_generator()
        else:
            num_images = len(self.obj.train_list)
            generator = self.obj.train_generator()
        true_positives = 0
        val_loss = 0
        print("\nEVALUATION STARTED")
        num_batches = num_images // self.batch_size if num_images % self.batch_size == 0 else num_images // self.batch_size + 1
        for i in range(num_batches):
            x_batch, y_batch = get_batch(generator, set_type, height=self.model.height,
                                         width=self.model.width, processing_type=preprocessing)

            predicted = sess.run([self.model.pred], feed_dict={self.model.x: x_batch, self.model.y: y_batch})

            true_positives = true_positives + np.sum(predicted[0] == np.argmax(y_batch, 1))

        val_accuracy = true_positives / num_images
        return val_accuracy

    # predict the labels for test dataset
    def predict(self, sess, set_type, preprocessing):
        if set_type == 'val':
            num_images = len(self.obj.val_list)
            generator = self.obj.val_generator()
        elif set_type == 'test':
            num_images = len(self.obj.test_list)
            generator = self.obj.test_generator()
        else:
            num_images = len(self.obj.train_list)
            generator = self.obj.train_generator()

        true_positives = 0
        num_batches = num_images // self.batch_size if num_images % self.batch_size == 0 else num_images // self.batch_size + 1
        model_predictions = []
        print("\nPREDICTION STARTED")
        for i in range(num_batches):
            x_batch, _ = get_batch(generator, set_type, height=self.model.height,
                                   width=self.model.width, processing_type=preprocessing)
            predicted = sess.run([self.model.pred], feed_dict={self.model.x: x_batch})
            model_predictions.extend(predicted[0])
        return model_predictions

    def run_training_testing(self, model_weight_path, model_name, device):

        # train the network

        config = tf.ConfigProto()

        for preprocessing in self.preprocessing:
            # load the model
            tf.reset_default_graph()
            if model_name == 'vgg':
                self.model = vgg16(weight_path=model_weight_path, train=1)
            elif model_name == 'alex':
                self.model = alex()
            train_generator_obj = self.obj.train_generator()
            with tf.Session(config=config) as sess:
                saver = tf.train.Saver(max_to_keep=1)
                self.model.optimize()
                val_accuracy_tf = tf.Variable(0, dtype=np.float64)
                tf.summary.scalar('validation accuracy', val_accuracy_tf)
                tf.summary.scalar("loss", self.model.loss)
                merged = tf.summary.merge_all()
                sess.run(tf.global_variables_initializer())
                if self.training:
                    print('PREPROCESSING METHOD: ', preprocessing, '\n')
                    writer = tf.summary.FileWriter(self.DIR + "/train")
                    writer.add_graph(sess.graph)
                    if self.restore:
                        print('RESTORING MODEL')
                        saver.restore(sess, tf.train.latest_checkpoint('./checkpoints/'))
                    else:
                        print('LOADING WEIGHTS')
                        self.model.load_weight(sess, model_weight_path)
                    print('to train: ',
                          self.model.param_finetune+self.model.param_train_complete)
                    np.set_printoptions(threshold=np.nan)
                    count = 0

                    for epochs in range(1, self.num_epochs + 1):
                        start_time = time.time()
                        print('start time:' + str(start_time))
                        for step in range(len(self.obj.train_list) // self.batch_size + 1):
                            count = count + 1
                            print("\n*********************")
                            print('epoch: ', epochs, '\nstep: ', count)
                            x_batch, y_batch = get_batch(train_generator_obj, 'train', height=self.model.height,
                                                         width=self.model.width, processing_type=preprocessing)
                            # for img in x_batch:
                            #     img_show = img[:, :, ::-1]
                            #     cv2.imshow('image', img_show)
                            #     cv2.waitKey(700)

                            proper_response = []
                            for l in y_batch:
                                proper_response.append(l.tolist().index(1))
                            summary, _, loss_curr, predicted, accuracy = sess.run([merged,
                                                                                    self.model.optimizer, self.model.loss,
                                                                                    self.model.pred,
                                                                                    self.model.accuracy],
                                                                                    feed_dict={self.model.x: x_batch,
                                                                                              self.model.y: y_batch})
                            # print('loss dash: \n', loss_dash)
                            print("loss: ", loss_curr)
                            true_positives = np.sum(predicted == np.argmax(y_batch, 1))
                            # accuracy = true_positives* 100.0 / y_batch.shape[0]
                            # a = np.matrix([proper_response, predicted])
                            # print("predicted: \n", a, '\n')
                            print('accuracy absolute:', true_positives)
                            print('accuracy:', accuracy)
                            print("*********************\n\n")
                            writer.add_summary(summary, global_step=step)

                        end_time = time.time()
                        print('time_taken', end_time - start_time)
                        print('epochs:', epochs, '\n\n\n')
                        saver.save(sess, './checkpoints'+str(preprocessing)+'/',
                                   global_step=step)
                        val_accuracy_actual = self.evaluate(sess, 'val', preprocessing)

                        value = tf.Variable(val_accuracy_actual)
                        sess.run(value.initializer)
                        assign = val_accuracy_tf.assign(value)
                        sess.run(assign)
                        print('\n#########\n', 'validation accuracy = ',
                              val_accuracy_actual, '\n\n')
                        print('')
        for testing in (3, 6, 7):
            # predict values for test dataset
            if self.testing == 1:
                config = tf.ConfigProto()
                print('\nTESTING METHOD: ', testing)
                # '''Prediction will be realised during 2nd phase of testing'''
                with tf.Session(config=config) as sess:
                    saver.restore(sess, tf.train.latest_checkpoint('./checkpoints/'))
                    model_pred = self.predict(sess, 'test', testing)
                # # save the results in the required csv format
                save_csv(model_pred, self.obj, testing)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--batch_size", help="batch_size", default=100, type=int)
    parser.add_argument("--val_ratio", help="validation-ratio", default=0.1, type=float)
    parser.add_argument("--num_epochs", help="num of iterations", default=75, type=int)
    parser.add_argument("--training", help="if train the model", default=1, type=int)
    parser.add_argument("--restore", help="if restore the model", default=0, type=int)
    parser.add_argument("--preprocessing", help="type of preprocessing", default=0, type=int)
    parser.add_argument("--device", help="which device", default="None", type=str)
    parser.add_argument("--model", help="model-name = vgg|alex|resnet", required=True, type=str)

    config = parser.parse_args()

    ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
    WEIGHTS_PATH = os.path.join(ROOT_DIR, 'weights')

    setup(ROOT_DIR=ROOT_DIR, model_name=config.model)

    if config.model == 'vgg':
        model_weight_path = os.path.join(WEIGHTS_PATH, 'vgg16_weights.npz')
    elif config.model == 'alex':
        model_weight_path = os.path.join(WEIGHTS_PATH, 'bvlc_alexnet.npy')
    else:
        print("No such model exsists")
        sys.exit(0)

    net = deep_neural_net(ROOT_DIR, config.batch_size, config.val_ratio,
                          config.num_epochs,
                          config.model, model_weight_path, config.training,
                          config.restore, config.preprocessing, config.device)