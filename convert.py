'''
# Convert the Dataset into the required format(break the train+test data files into separate files) required by the CNN-code
import csv, random
import numpy as np
#store train and test image ids
train_image_ids = []
test_image_ids = []



images_labels = [[] for i in range(200)]
with open('data/image_class_labels.txt') as labels_file:
    spamreader = csv.reader(labels_file, delimiter=' ')
    for row in spamreader:
        index = int(row[1])
        images_labels[index-1].append(int(row[0]))


with open('data/train_test_split.txt') as train_test_file:
    for image_id,line in enumerate(train_test_file):
        is_train = int(line.strip().split()[1])
        if is_train == 1:
            train_image_ids.append(image_id+1)
        else:
            test_image_ids.append(image_id+1)

grouped_test_ids = [[] for i in range(200)]
for i in range(200):
    for j in range(len(test_image_ids)):
        if test_image_ids[j] in (images_labels[i]):
            grouped_test_ids[i].append(test_image_ids[j])

test_image_ids = []
for i in range(200):
    group_size = len(grouped_test_ids[i])
    part = int(group_size * 0.59)
    random_part = random.sample(grouped_test_ids[i], part)
    grouped_test_ids[i] = [x for x in grouped_test_ids[i] if x not in random_part]
    train_image_ids.extend(random_part)
    test_image_ids.extend(grouped_test_ids[i])

print(np.array(grouped_test_ids).shape)
print(len(train_image_ids))
print(len(test_image_ids))
print(len(train_image_ids) / (len(train_image_ids) + len(test_image_ids)))



#Splitting Images into two files -- images_train.txt and images_test.txt
with open('data/images.txt') as image_locations, open('data/images_test.txt', "w") as test_image_locations, open('data/images_train.txt', "w") as train_image_locations:
    for image_id, line in enumerate(image_locations):
        #print(image_id+1,line.strip().split()[1],)
        if image_id+1 in train_image_ids:
            train_image_locations.write(line)
        else:
            test_image_locations.write(line)



#Splitting Bounding Boxes into two files -- train_labels.txt and test_labels.txt
with open('data/bounding_boxes.txt') as image_locations, open('data/bounding_boxes_test.txt', "w") as test_image_bounding_boxes, open('data/bounding_boxes_train.txt', "w") as train_image_bounding_boxes:
    for image_id, line in enumerate(image_locations):
        #print(image_id+1,line.strip().split()[1],)
        if image_id+1 in train_image_ids:
            train_image_bounding_boxes.write(line)
        else:
            test_image_bounding_boxes.write(line)


#Splitting Labels into two files -- train_labels.txt and test_labels.txt
with open('data/image_class_labels.txt') as image_locations, open('data/image_class_labels_test.txt', "w") as test_image_class, open('data/image_class_labels_train.txt', "w") as train_image_class:
    for image_id, line in enumerate(image_locations):
        #print(image_id+1,line.strip().split()[1],)
        if image_id+1 in train_image_ids:
            train_image_class.write(line)
        else:
            test_image_class.write(line)

'''
import os
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
IMGS_PATH = os.path.join(ROOT_DIR, 'data', 'images')

import cv2
import numpy as np

number = sum(1 for line in open('data/images_train.txt', "r"))
print('number: ', number)
with open('data/images_train.txt', "r") as train_image_locations:
    r = 0
    g = 0
    b = 0
    for image_id, line in enumerate(train_image_locations):
        img = cv2.imread(os.path.join(IMGS_PATH, str(line.strip().split()[1])))
        im_size = img.shape[0] * img.shape[1]
        r = r + (np.sum(img[:, :, 2]) / im_size)
        g = g + (np.sum(img[:, :, 1]) / im_size)
        b = b + (np.sum(img[:, :, 0]) / im_size)
        print(image_id)
        # cv2.imshow('im', img)
        # cv2.waitKey(50)
    print ('R: ', r / number)
    print ('G: ', g / number)
    print ('B: ', b / number)

